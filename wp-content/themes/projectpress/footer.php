		<?php get_sidebar( 'footer' ); ?>

		<div id="footer-bottom">
		<?php
			$menu_class = 'bottom-nav';
			$footerNav = '';

			$footerNav = wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menu_class, 'echo' => false, 'depth' => '1' ) );

			if ( '' === $footerNav )
				show_page_menu( $menu_class );
			else
				echo( $footerNav );
		?>
		</div> <!-- #footer-bottom -->
	</div> <!-- .page-wrap -->

	<div id="footer-info" class="container">
		<p id="copyright"><?php printf( __( 'Designed by %1$s and %2$s | Powered by %3$s', 'Nexus' ), '<a href="#" title="hackUniTO team">#hackUniTO team</a>', '<a href="#" title="top-ix">Top-ix</a>', '<a href="http://www.wordpress.org">WordPress</a>' ); ?></p>
	</div>

	<?php wp_footer(); ?>
</body>
</html>